#include <stdio.h>
#include <stdlib.h>
#include "lodepng.h"

typedef struct myimage{
    unsigned width, height;
    unsigned char* canal_rouge;
    unsigned char* canal_vert;
    unsigned char* canal_bleu;
    unsigned char* canal_alpha;
}myimage;

void encodeOneStep(const char* filename, const unsigned char* image, unsigned width, unsigned height)
{
  /*Encode the image*/
  unsigned error = lodepng_encode32_file(filename, image, width, height);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));
}

void decodeOneStep(const char* filename)
{
  unsigned error;
  unsigned char* image;
  unsigned width, height;

  error = lodepng_decode32_file(&image, &width, &height, filename);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  /*use image here*/


  free(image);
}

myimage LireImage(char* nom_fichier){
    unsigned error;
    unsigned char* image;
    unsigned width, height;

    error = lodepng_decode32_file(&image, &width, &height, nom_fichier);
    if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

    /*use image here*/
    myimage mastruct={0,0,NULL,NULL,NULL,NULL};
    mastruct.width=width;
    mastruct.height=height;
    // Allouer de la mémoire pour les canaux.
    mastruct.canal_rouge = (unsigned char*)malloc(width * height * sizeof(unsigned char));
    mastruct.canal_vert = (unsigned char*)malloc(width * height * sizeof(unsigned char));
    mastruct.canal_bleu = (unsigned char*)malloc(width * height * sizeof(unsigned char));
    mastruct.canal_alpha = (unsigned char*)malloc(width * height * sizeof(unsigned char));
    // Copier les données de l'image décodée dans les canaux.
    for (unsigned y = 0; y < height; ++y) {
        for (unsigned x = 0; x < width; ++x) {
            mastruct.canal_rouge[y * width + x] = image[4 * (y * width + x) + 0];
            mastruct.canal_vert[y * width + x] = image[4 * (y * width + x) + 1];
            mastruct.canal_bleu[y * width + x] = image[4 * (y * width + x) + 2];
            mastruct.canal_alpha[y * width + x] = image[4 * (y * width + x) + 3];
        }
    }
    
    //encodeOneStep(fichier_sortie, image,width, height);

    free(image);
    return mastruct;
}

void EcrireImage(myimage im, char* nom_fichier) {
    unsigned char* image = (unsigned char*)malloc(im.width * im.height * 4 * sizeof(unsigned char));
    
    if (!image) {
        printf("Erreur d'allocation mémoire\n");
        return;
    }

    // Copier les données des canaux dans le tableau image.
    for (unsigned y = 0; y < im.height; ++y) {
        for (unsigned x = 0; x < im.width; ++x) {
            image[4 * (y * im.width + x) + 0] = im.canal_rouge[y * im.width + x];
            image[4 * (y * im.width + x) + 1] = im.canal_vert[y * im.width + x];
            image[4 * (y * im.width + x) + 2] = im.canal_bleu[y * im.width + x];
            image[4 * (y * im.width + x) + 3] = im.canal_alpha[y * im.width + x];
        }
    }

    // Encodez et écrivez l'image dans le fichier.
    encodeOneStep(nom_fichier, image, im.width, im.height);

    // Libérer la mémoire du tableau image.
    free(image);
}
void NoirEtBlanc(myimage im) {
    for (unsigned y = 0; y < im.height; ++y) {
        for (unsigned x = 0; x < im.width; ++x) {
            // Calculer la moyenne des canaux RGB.
            unsigned char moyenne = (im.canal_rouge[y * im.width + x] +
                                      im.canal_vert[y * im.width + x] +
                                      im.canal_bleu[y * im.width + x]) / 3;

            // Affecter la même valeur à tous les canaux (R, G, B).
            im.canal_rouge[y * im.width + x] = moyenne;
            im.canal_vert[y * im.width + x] = moyenne;
            im.canal_bleu[y * im.width + x] = moyenne;
        }
    }
}


int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Utilisation : %s <fichier_entree> <fichier_sortie>\n", argv[0]);
        return 1;
    }

    char *fichier_entree = argv[1];
    char *fichier_sortie = argv[2];

    myimage im = LireImage(fichier_entree);

    if (im.width == 0 || im.height == 0) {
        printf("Erreur lors de la lecture de l'image.\n");
        return 1;
    }

    NoirEtBlanc(im); // Convertir l'image en noir et blanc.

    EcrireImage(im, fichier_sortie);

    // Libérer la mémoire des canaux de l'image.
    free(im.canal_rouge);
    free(im.canal_vert);
    free(im.canal_bleu);
    free(im.canal_alpha);

    return 0;
}

